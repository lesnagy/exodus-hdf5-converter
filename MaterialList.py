#import netCDF4 as ncdf

from scipy.io import netcdf as ncdf
import numpy

from Material import *

#------------------------------------------------------------------------------
# A list of materials.
#------------------------------------------------------------------------------
class MaterialList:

  #----------------------------------------------------------------------------
  # Default constructor, does nothing.
  #----------------------------------------------------------------------------
  def __init__(self, ncfile):

    # NetCDF file object
    self.ncfile = ncfile

    # Materials
    self.materials = {}

    # Material names (kept in the correct order).
    self.material_names = []

    # The type of cells.
    self.cell_type = None

    # Geometric dimension.
    self.geometric_dim = ncfile.dimensions['num_dim']

    # Total number of cells.
    self.num_cells = ncfile.dimensions['num_elem']

    # Total number of vertices.
    self.num_vertices = ncfile.dimensions['num_nodes']

    # X,Y & Z coordinates for vertices.
    self.x_coords = ncfile.variables['coordx']
    self.y_coords = ncfile.variables['coordy']
    self.z_coords = ncfile.variables['coordz']

    # An index that is used to reference the NetCDF variables:
    #    * num_el_in_blk - Number of elements in a block (below).
    #    * connect       - Connectivity information (below).
    i = 0

    # Element block start index.
    elem_map_start = 0

    # Element block end index.
    elem_map_end = 0
    
    # Element map. This is a list of indices which are grouped in to blocks.
    # Each element name corresponds to a block starting at elem_map_start to 
    # elem_map_end (inclusive).
    elem_map = ncfile.variables['elem_map']

    # Material name.
    matnames = ncfile.variables['eb_names']

    # Loop round each of the material names.
    material_type = None
    for matname in matnames:

      # Extract material name as a string.
      str_matname = ''.join(matname)
      print("Found material: '{matName}'".format(matName=str_matname))

      # Add the string version of the material name to this objects list of
      # material names.
      self.material_names.append(str_matname)

      # Extract the number of elements in block i (correspoinding to the 
      # material name).
      num_el_in_blk = ncfile.dimensions['num_el_in_blk%i' % (i+1)]
      
      # Create a material.
      mat = Material()

      # Set name, index, connectivity (node indices for actual elements) and 
      # number of elements for the material.
      mat.name     = str_matname
      mat.idx      = i+1
      mat.connect  = ncfile.variables['connect%i' % (i+1)]
      mat.nelems   = num_el_in_blk
      mat.type     = ncfile.variables['connect{i}'.format(i=(i+1))].elem_type

      # Check material type for inconsistency.
      if material_type:
        if mat.type != material_type:
            print "WARNING: material type difference! Was '{to}', is '{tn}'".format(
              to = material_type, tn = mat.type)
      
      material_type = mat.type

      elem_map_end += num_el_in_blk
      mat.elements  = elem_map[elem_map_start:elem_map_end]

      self.materials[str_matname] = mat

      elem_map_start += num_el_in_blk
      i = i + 1

    if material_type == 'TETRA':
      self.cell_type = 'tetrahedron'

  #----------------------------------------------------------------------------
  # Return the material associated with a name.
  #----------------------------------------------------------------------------
  def get_material(self, name):

    return self.materials[name]

  #----------------------------------------------------------------------------
  # Return a list of available materials (in the correct order).
  #----------------------------------------------------------------------------
  def available(self):

    return self.material_names

