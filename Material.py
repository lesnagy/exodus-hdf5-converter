#------------------------------------------------------------------------------
# Class to hold information associated with a material.
#------------------------------------------------------------------------------
class Material:

  #----------------------------------------------------------------------------
  # Default constructor, create a default material with no information.
  #----------------------------------------------------------------------------
  def __init__(self):

    self.name = ''
    self.idx = -1
    self.connect = None
    self.nelems = -1
    self.elements = ''
    self.type = None

  #----------------------------------------------------------------------------
  # Return a string representation of this object.
  #----------------------------------------------------------------------------
  def __repr__(self):
    return "('{name}', {idx}, {nelems}, '{type}')".format(
      name   = self.name, 
      idx    = self.idx, 
      nelems = self.nelems, 
      type   = self.type)

